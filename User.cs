﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blundr
{
    /// <summary>
    /// This is an implementation of class"User",to represent a
    /// User in my implementation of a  Application
    /// </summary>


    internal class User

    {

        public string name;                  //Representation of User name;
        public int age;                      //Representation of age;
        private string qualification;        //Representation of qualification;
        public string interest;              //Representation of interesr;
        private string allinterests;         //Representation of allinterests;


        //declaring a constructor for User class.

        public User(string name, int age, string qualification, string interest, string allinterests)
        {

            //this.user_id = user_id;
            this.name = name;
            this.age = age;
            this.qualification = qualification;
            this.interest = interest;
            this.allinterests = allinterests;
            Console.WriteLine(this);


        }

        /**
        * This is a override of tostring method to print the Message object in
        * human readable form.
        */

        override
        public String ToString()

        {
            String stringToReturn = "This is a User object details: \n";

            stringToReturn += " name:               " + this.name + "\n";
            stringToReturn += " age  :              " + this.age + "\n";
            stringToReturn += " qualification:      " + this.qualification + "\n";
            stringToReturn += " interest:           " + this.interest + "\n";
            stringToReturn += " allinterts          " + this.allinterests + "\n";

            return stringToReturn;

        }
    }
}






        
    
